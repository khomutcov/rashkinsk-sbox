﻿using Sandbox.Physics;

namespace Sandbox.Jobs;

[Library( "job_police", Title = "Police", Description = "Kill thugs")]
public partial class Police : BaseJob
{
	public override int Salary { get; } = 1000;
	
	public override void Activate()
	{
		base.Activate();
	}
}
