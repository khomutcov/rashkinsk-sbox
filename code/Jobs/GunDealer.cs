﻿using Sandbox.Physics;

namespace Sandbox.Jobs;

[Library( "job_gundealer", Title = "Gun Dealer", Description = "Sell guns!")]
public partial class GunDealer : BaseJob
{
	public override int Salary { get; } = 250;
	
	public override void Activate()
	{
		base.Activate();
	}
}
