﻿using Sandbox.Physics;

namespace Sandbox.Jobs;

[Library( "job_citizen", Title = "Citizen", Description = "Choose another job")]
public partial class Citizen : BaseJob
{
	public override void Activate()
	{
		base.Activate();
	}
}
