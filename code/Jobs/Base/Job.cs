﻿namespace Sandbox.Jobs
{
	public partial class BaseJob : BaseNetworkable
	{
		public virtual int Salary { get; } = 100;
		// Зарплата
		// Список оружия (отдельно список дефолтного и уникального)
		// Список доступных RPEntities (отдельно дефолтного и уникального)

		public virtual void Activate()
		{
			Log.Info( "activate " + ClassName );
		}

		public virtual void Deactivate()
		{
		}

		protected void DespawnWeapons()
		{
			
		}

		protected void SpawnWeapons()
		{
			
		}
	}
}
