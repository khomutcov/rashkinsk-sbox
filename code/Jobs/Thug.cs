﻿using Sandbox.Physics;

namespace Sandbox.Jobs;

[Library( "job_thug", Title = "Thug", Description = "Kill police")]
public partial class Thug : BaseJob
{
	public virtual int Salary { get; } = 150;
	
	public override void Activate()
	{
		base.Activate();
	}
}
