﻿namespace Sandbox.RPCommands;

[Library( "drop_money", Title = "Drop Money", Description = "Drop money on the ground")]
public partial class DropMoney : BaseRPCommand
{
	[ConCmd.Server("drop_money")]
	protected static void CommandDropMoney(int amount)
	{
		Log.Info( "Use" );
		var player = ConsoleSystem.Caller.Pawn as SandboxPlayer;
		if (player == null) return;

		player.AddMoney( -amount );
	}
}
