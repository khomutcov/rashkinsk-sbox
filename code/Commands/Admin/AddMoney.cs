﻿namespace Sandbox.Commands.Admin
{
	public class AddMoney
	{
		[ConCmd.Server( "add_money" )]
		private static void CommandAddMoney(string playerName, int amount)
		{
			foreach ( var client in Game.Clients )
			{
				if ( client.Name == playerName )
				{
					var player = client.Pawn as SandboxPlayer;
					if ( player == null ) return;
					
					player.AddMoney( amount );

					return;
				}
			}
			
			Log.Info( "Player wasn't found" );
		}
	}
}
