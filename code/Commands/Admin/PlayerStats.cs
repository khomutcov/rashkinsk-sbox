﻿namespace Sandbox.Commands.Admin
{
	public class PlayerStats
	{
		[ConCmd.Server( "player_stats_server" )]
		private static void ShowStatsServer()
		{
			var player = ConsoleSystem.Caller.Pawn as SandboxPlayer;
			if ( player == null ) return;

			Log.Info( player.CurrentJob.ClassName );
			Log.Info( player.Money );
		}

		[ConCmd.Client( "player_stats_client" )]
		private static void ShowStatsClient()
		{
			var player = ConsoleSystem.Caller.Pawn as SandboxPlayer;
			if ( player == null ) return;

			Log.Info( player.CurrentJob.ClassName );
			Log.Info( player.Money );
		}
	}
}
