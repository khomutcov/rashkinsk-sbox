﻿namespace Sandbox.Commands.Admin
{
	public class ClearStats
	{
		[ConCmd.Server( "clear_stats" )]
		private static void CommandClearStats(string playerName)
		{
			foreach ( var client in Game.Clients )
			{
				Log.Info("CL :" + client.Name+"; " + playerName);
				if ( client.Name == playerName )
				{
					var player = client.Pawn as SandboxPlayer;
					if ( player == null ) return;
					
					Log.Info( player.CurrentJob.ClassName );
					Log.Info( player.Money );
					player.Clear();
					Log.Info( player.CurrentJob.ClassName );
					Log.Info( player.Money );

					return;
				}
			}
			
			Log.Info( "Player wasn't found" );
		}
	}
}
