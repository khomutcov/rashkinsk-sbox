﻿using Sandbox;

partial class SandboxPlayer
{
	#region Fields
	
	/// <summary>
	/// Максимальное количество брони
	/// </summary>
	private readonly float MaxArmor = 100;
	
	#endregion
	
	#region Properties

	/// <summary>
	/// Количество очков защиты
	/// </summary>
	[Net]
	public float Armor { get; private set; }

	#endregion

	#region Public

	public void AddArmor(float value)
	{
		if ( Armor + value < 0 )
			Armor = 0;
		else if ( Armor + value > MaxArmor )
			Armor = MaxArmor;
		else 
			Armor += value;
	}
	
	#endregion
}
