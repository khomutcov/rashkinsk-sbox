﻿using Sandbox;

partial class SandboxPlayer
{
	#region Fields

	/// <summary>
	/// Маскимальное количество очков еды
	/// </summary>
	private readonly float MaxFood = 100;
	
	#endregion

	#region Properties

	/// <summary>
	/// Количество очков еды
	/// </summary>
	[Net, Change]
	public float Food { get; private set; }

	#endregion

	#region Private

	private void OnFoodChanged( float oldValue, float newValue )
	{
		CompleteSave();
	}

	#endregion
	
	#region Public

	public void AddFood(float value)
	{
		if ( Food + value < 0 )
			Food = 0;
		else if ( Food + value > MaxFood )
			Food = MaxFood;
		else 
			Food += value;
	}
	
	#endregion
}
