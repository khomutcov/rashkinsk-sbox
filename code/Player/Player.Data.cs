﻿using Sandbox;

partial class SandboxPlayer
{
	#region InnerClasses

	public class PlayerData
	{
		public int Money { get; init; }
		
		public float Food { get; init; }
		
		public string RPName { get; init; }
	}

	#endregion

	#region Save/Load

	/// <summary>
	/// Сохранение всех данных игрока
	/// </summary>
	public void CompleteSave()
	{
		/*
		PlayerData data = new PlayerData { Money = Money, Food = Food, RPName = RPName };

		Save(data);
		*/
	}

	/// <summary>
	/// Загрузка всех данных игрока
	/// </summary>
	public void CompleteLoad()
	{
		/*
		PlayerData data = Load();

		Money = data?.Money ?? 0;
		Food = data?.Food ?? 100f;
		RPName = data?.RPName ?? "";
		*/
	}
	
	/// <summary>
	/// Сохранение данных игрока
	/// </summary>
	private void Save( PlayerData data )
	{
		FileSystem.Data.WriteJson( "player_data.json", data );
	}

	/// <summary>
	/// Загрузка данных игрока
	/// </summary>
	private PlayerData Load()
	{
		return FileSystem.Data.ReadJson<PlayerData>( "player_data.json" );
	}

	/// <summary>
	/// Очистка данных игрока
	/// </summary>
	public void Clear()
	{
		FileSystem.Data.DeleteFile( "player_data.json" );
		
		CompleteLoad();
	}
	
	#endregion
}
