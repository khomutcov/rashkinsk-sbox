﻿using Sandbox;

partial class SandboxPlayer
{
	#region Fields

	/// <summary>
	/// Ролеплей имя
	/// </summary>
	private string rpName;
	
	#endregion

	#region Properties

	/// <summary>
	/// Ролеплей имя
	/// </summary>
	public string RPName
	{
		get
		{
			return rpName;
		}
		set
		{
			rpName = value;
			// TODO: Переместить сюда проверки, которые будут использоваться в ChangeRPName
		}
	}

	#endregion

	#region public

	/// <summary>
	/// Сменить ролеплей имя
	/// </summary>
	/// <param name="_name">Новое имя</param>
	/// <returns>Результат смены</returns>
	public bool ChangeRPName(string _name)
	{
		rpName = _name;
		
		// TODO: Различные проверки на длинну, формат и т. д.
		return true;
	}

	#endregion
}
