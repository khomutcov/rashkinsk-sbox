﻿using System.Threading.Tasks;
using Sandbox;
using Sandbox.Jobs;

partial class SandboxPlayer
{
	#region Fields

	/// <summary>
	/// Время для получения зарплаты в секундах
	/// </summary>
	private readonly int salaryTime = 10;

	#endregion
	
	#region Properties

	[ConVar.ClientData( "job_current" )]
	public static string UserJobCurrent { get; set; } = "job_citizen";

	[Net]
	public BaseJob CurrentJob { get; set; } = new Citizen();
	
	#endregion

	#region Private

	private async void InitializeSalaryTask()
	{
		while ( true )
		{
			await GameTask.RunInThreadAsync( SalaryTask );
		}
	}

	private async Task SalaryTask()
	{
		await GameTask.Delay( 1000 * salaryTime );
		GiveSalary();
	}

	/// <summary>
	/// Выдать зарплату
	/// </summary>
	private void GiveSalary()
	{
		AddMoney( CurrentJob.Salary );
	}
	
	private void UpdateCurrentJob( IClient owner )
	{
		var jobName = owner.GetClientData<string>( "job_current", "job_citizen" );
		if ( jobName == null )
			return;

		// Already the right job
		if ( CurrentJob != null && CurrentJob.ClassName == jobName )
			return;

		if ( CurrentJob != null )
		{
			CurrentJob?.Deactivate();
			CurrentJob = null;
		}

		CurrentJob = TypeLibrary.Create<BaseJob>( jobName );

		if ( CurrentJob != null )
		{
			CurrentJob.Activate();
		}
	}
	
	#endregion
}
