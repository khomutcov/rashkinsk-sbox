﻿using Sandbox;

partial class SandboxPlayer
{
	#region Fields
	
	/// <summary>
	/// Максимальное количество денег
	/// </summary>
	private readonly int MaxMoney = 10000000;
	
	#endregion
	
	#region Properties

	/// <summary>
	/// Количество денег
	/// </summary>
	[Net, Change]
	public int Money { get; private set; }

	#endregion

	#region Private
	
	private void OnMoneyChanged( int oldValue, int newValue )
	{
		CompleteSave();
	}
	
	#endregion
	
	#region Public

	/// <summary>
	/// Изменение количества денег. Передавать отрицательные значения, если необходимо уменьшить значение.
	/// </summary>
	public void AddMoney(int value)
	{
		if ( Money + value < 0 )
			Money = 0;
		else if ( Money + value > MaxMoney )
			Money = MaxMoney;
		else 
			Money += value;
	}

	#endregion
}
