﻿using System;
using Sandbox;
using Sandbox.Jobs;
using Sandbox.RPCommands;
using Sandbox.Tools;
using Sandbox.UI;
using Sandbox.UI.Construct;

[Library]
public partial class SpawnMenu : Panel
{
	public static SpawnMenu Instance;
	private readonly Panel toolList;
	private readonly Panel jobsList;
	private readonly Panel commandsList;

	private bool isSearching;
	private TimeSince TimeSinceActiveSlotChanged;

	public SpawnMenu()
	{
		Instance = this;

		var left = Add.Panel( "left" );
		{
			var tabs = left.AddChild<ButtonGroup>();
			tabs.AddClass( "tabs" );

			var body = left.Add.Panel( "body" );
			{
				var spawnList = body.AddChild<SpawnList>();
				tabs.SelectedButton = tabs.AddButtonActive( "#spawnmenu.props", ( b ) => spawnList.SetClass( "active", b ) );

				commandsList = body.Add.Panel( "rpcommandslist" );
				{
					RebuildCommandsList();
				}
				tabs.AddButtonActive( "#spawnmenu.rpcommands", ( b ) => commandsList.SetClass( "active", b ) );
				
				jobsList = body.Add.Panel( "jobslist" );
				{
					RebuildJobsList();
				}
				tabs.AddButtonActive( "#spawnmenu.jobs", ( b ) => jobsList.SetClass( "active", b ) );

				var rpEntities = body.AddChild<RPEntitiesList>();
				tabs.AddButtonActive( "#spawnmenu.rpentities", ( b ) => rpEntities.SetClass( "active" , b ) );
			}
		}

		var right = Add.Panel( "right" );
		{
			var tabs = right.Add.Panel( "tabs" );
			{
				tabs.Add.Button( "#spawnmenu.tools" ).AddClass( "active" );
				tabs.Add.Button( "#spawnmenu.utility" );
			}
			var body = right.Add.Panel( "body" );
			{
				toolList = body.Add.Panel( "toollist" );
				{
					RebuildToolList();
				}
				body.Add.Panel( "inspector" );
			}
		}
		
		// TODO: Обратить внимание, вдруг будут проблемы без отписки
		InventoryBar.OnActiveSlotChanged -= OnActiveSlotChanged;
		InventoryBar.OnActiveSlotChanged += OnActiveSlotChanged;
	}

	private void RebuildCommandsList()
	{
		commandsList.DeleteChildren( true );

		foreach ( var entry in TypeLibrary.GetTypes<BaseRPCommand>() )
		{
			if ( entry.Name == "BaseRPCommand" )
				continue;

			var button = commandsList.Add.Button( entry.Title );

			button.AddEventListener( "onclick", () => 
			{
				UseRPCommand( entry.ClassName );

				foreach ( var child in commandsList.Children )
					child.SetClass( "active", child == button );
			} );
		}
	}
	
	private void RebuildJobsList()
	{
		jobsList.DeleteChildren( true );

		foreach ( var entry in TypeLibrary.GetTypes<BaseJob>() )
		{
			if ( entry.Name == "BaseJob" )
				continue;

			var button = jobsList.Add.Button( entry.Title );
			button.SetClass( "active", entry.ClassName == ConsoleSystem.GetValue( "job_current" ) );

			button.AddEventListener( "onclick", () => 
			{
				SetActiveJob( entry.ClassName );

				foreach ( var child in jobsList.Children )
					child.SetClass( "active", child == button );
			} );
		}
	}
	
	private void RebuildToolList()
	{
		toolList.DeleteChildren( true );

		foreach ( var entry in TypeLibrary.GetTypes<BaseTool>() )
		{
			if ( entry.Name == "BaseTool" )
				continue;

			var button = toolList.Add.Button( entry.Title );
			button.SetClass( "active", entry.ClassName == ConsoleSystem.GetValue( "tool_current" ) );

			button.AddEventListener( "onclick", () => 
			{
				SetActiveTool( entry.ClassName );

				foreach ( var child in toolList.Children )
					child.SetClass( "active", child == button );
			} );
		}
	}

	private void UseRPCommand( string className )
	{
		Log.Info( "USERPCOMMAND " + className );
		ConsoleSystem.Run( className );
	}
	
	private void SetActiveJob( string className )
	{
		// setting a cvar
		ConsoleSystem.SetValue( "job_current", className );
	}
	
	void SetActiveTool( string className )
	{
		// setting a cvar
		ConsoleSystem.SetValue( "tool_current", className );

		// set the active weapon to the toolgun
		if ( Game.LocalPawn is not Player player ) return;
		if ( player.Inventory is null ) return;

		// why isn't inventory just an ienumurable wtf
		for ( int i = 0; i < player.Inventory.Count(); i++ )
		{
			var entity = player.Inventory.GetSlot( i );
			if ( !entity.IsValid() ) continue;
			if ( entity.ClassName != "weapon_tool" ) continue;

			player.ActiveChildInput = entity;
		}
	}

	private void OnActiveSlotChanged()
	{
		Parent.SetClass( "onslotchanged", true );
		TimeSinceActiveSlotChanged = 0;
	}

	public override void Tick()
	{
		base.Tick();
		
		UpdateActiveTool();
		UpdateActiveJob();
		
		if ( isSearching )
			return;

		Parent.SetClass( "spawnmenuopen", Input.Down( "menu" ) );

		if ( TimeSinceActiveSlotChanged >= 2.5f )
		{
			Parent.SetClass( "onslotchanged", false );
		}
	}

	void UpdateActiveTool()
	{
		var toolCurrent = ConsoleSystem.GetValue( "tool_current" );
		var tool = string.IsNullOrWhiteSpace( toolCurrent ) ? null : TypeLibrary.GetType<BaseTool>( toolCurrent );

		foreach ( var child in toolList.Children )
		{
			if ( child is Button button )
			{
				child.SetClass( "active", tool != null && button.Text == tool.Title );
			}
		}
	}

	void UpdateActiveJob()
	{
		var jobCurrent = ConsoleSystem.GetValue( "job_current" );
		var job = string.IsNullOrWhiteSpace( jobCurrent ) ? null : TypeLibrary.GetType<BaseJob>( jobCurrent );

		foreach ( var child in jobsList.Children )
		{
			if ( child is Button button )
			{
				child.SetClass( "active", job != null && button.Text == job.Title );
			}
		}
	}

	public override void OnHotloaded()
	{
		base.OnHotloaded();

		RebuildToolList();
	}
}
